import './Button.css';

const Button = ({ buttonText, onClick, type = 'button' }) => {
	return (
		<button className='btn' onClick={onClick} type={type}>
			{buttonText}
		</button>
	);
};
export default Button;
