import './Input.css';

const Input = ({
	labelText,
	placeholderText,
	onChange,
	type = 'text',
	value,
}) => {
	return (
		<form className='form'>
			<label htmlFor='input'>{labelText}</label>
			<input
				className='input'
				onChange={onChange}
				type={type}
				name='input'
				placeholder={placeholderText}
				value={value}
			></input>
		</form>
	);
};

export default Input;
