import { useState } from 'react';
import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';

import { SEARCH_BUTTON_TEXT } from '../../../../constants';

import './SearchBar.css';

const SearchBar = ({ onUpdateSearch }) => {
	const [term, setTerm] = useState('');
	const onUpdateInput = (event) => {
		const term = event.target.value;
		setTerm(term);
		if (term.length === 0) onUpdateSearch(term);
	};
	const onUpdateSearchLocal = () => {
		onUpdateSearch(term);
	};
	return (
		<div className='search-container'>
			<Input
				placeholderText={'Enter course name...'}
				onChange={(event) => onUpdateInput(event)}
			/>
			<Button buttonText={SEARCH_BUTTON_TEXT} onClick={onUpdateSearchLocal} />
		</div>
	);
};

export default SearchBar;
