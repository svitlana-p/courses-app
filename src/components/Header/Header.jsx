import './Header.css';
import Button from '../../common/Button/Button';
import Logo from './components/Logo/Logo';

import { LOGOUT_BUTTON_TEXT } from '../../constants.js';

const Header = () => {
	return (
		<header className='header'>
			<Logo />
			<div className='header__container'>
				<p className='header__text'>Dave</p>
				<Button buttonText={LOGOUT_BUTTON_TEXT} />
			</div>
		</header>
	);
};
export default Header;
