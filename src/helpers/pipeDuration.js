export const pipeDuration = (duration) => {
	const hours = Math.floor(duration / 60);
	return hours + ':' + (duration - hours * 60);
};
